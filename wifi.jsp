<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.IOException"%>
<!DOCTYPE html>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<meta charset="UTF-8"/>
<title> 전국무료와이파이표준데이터_강민규 </title>
<script>
var d = new Date();	// 날짜 변수 선언
</script>
</head>
<body>
<%
		File f = new File("/var/lib/tomcat8/webapps/ROOT/c04_project/wifisource.txt");
		BufferedReader br = new BufferedReader(new FileReader(f));

		String readtxt;

		if ((readtxt = br.readLine()) == null) {
			out.println("빈 파일입니다.");	
			return;
		}
		String[] field_name = readtxt.split("\t");	

		double lat = 37.385782;		
		double lng = 127.1211778;	
		double dist = 0;
		int LineCnt = 0;
		int fromPT = 0;
				
		int cntPT = 10;
		int startpage =0;
		int currentpage = fromPT;
		
		if(currentpage<6) {
			startpage=0;
		} else if(currentpage>(18659/cntPT)-4){
			startpage=(18659/cntPT)-9;
		} else {
			for (int i=0; i<(currentpage-5); i++) {
				startpage++;
			}
		}		
		
		int endpage;
		if(currentpage>(18659/cntPT)-5) {
			endpage=((18659/cntPT)+1);
		}
		else if(currentpage<5){
			endpage=10;
		}else{
			endpage=currentpage+5;
		}		
%>
<%!
public static double distFrom(double d, double e, double lat, double lng) {
	    double earthRadius = 6371000;
	    double dLat = Math.toRadians(lat-d);
	    double dLng = Math.toRadians(lng-e);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(d)) * Math.cos(Math.toRadians(lat)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = (float) (earthRadius * c);

	    return dist;
}
%>
<center>
<br>
<h1> KSW01_전국무료와이파이표준데이터 </h1><br>
<br>

<table style="table-layout:fixed; word-break:break-all;">
	<tr>
		<td class="view"><h4>CSV File : /var/lib/tomcat8/webapps/ROOT/c04_project/wifisource.txt</h4></td>
	</tr><tr></tr>
	<tr>
		<td class="view">page : <% out.println(currentpage); %> </td>
		<td id="end"><script> document.write(d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes());</script></td>
	</tr>
</table><br>

<TABLE border=1 cellspacing=0 style="table-layout:fixed; word-break:break-all;">
	<tr>
		<th width=3% align=center> 번호 </th>
		<th width=28% align=center> 지번주소 </th>
		<th width=28% align=center> 도로명주소 </th>
		<th width=10% align=center> 위도 </th>
		<th width=10% align=center> 경도 </th>
		<th width=12% align=center> 거리 </th>
	</tr>
	
<%		
		while ((readtxt = br.readLine()) != null) {
			if(LineCnt<fromPT) {
				LineCnt++;
				continue;
			}
			if(LineCnt>fromPT+cntPT) {
				break;
			}
			out.println("<tr>");
			String[] field = readtxt.split("\t");
			out.println("<td align=center>"+LineCnt+"</td>");	
			out.println("<td align=center>"+field[9]+"</td>");
			out.println("<td align=center>"+field[8]+"</td>");			
			out.println("<td align=center>"+field[12]+"</td>");	
			out.println("<td align=center>"+field[13]+"</td>");
			dist = distFrom(Double.parseDouble(field[12]), Double.parseDouble(field[13]), lat, lng);
			out.println("<td align=center>"+Math.round(dist/10)/100.0+"km</td>");
			out.print("</tr>");
			
			
			LineCnt++;
			
		}
		br.close();
		
%>
</TABLE>
<br>
<form method = "get" action="wifiaction.jsp">
	<select name= "list">
		<option value='10' selected> 10
		<option value='20'> 20
		<option value='50'> 50
		<option value='100'> 100
	</select>	
	<input type = "submit" name = "from" value = "<<">
<%
	for(int i=startpage; i<endpage; i++) {
		out.println("<input type = submit name = from value = " + (i+1) +">");
	}

%>
<input type = "submit"  name = "from" value = ">>">
</form>
</center>
</body>
</html>